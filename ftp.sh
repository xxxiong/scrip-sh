#!/bin/bash
ftp_file=/test/test.sql	    # 下载ftp上文件， 以及上传目录
local_file=/home/test.html  # 上传文件以及下载目录
ftpu='ftp'                  # ftp用户
ftpp='ftp'                  # ftp密码

ftp -n <<EOF
open 192.168.133.7 8090
user $ftpu $ftpp
binary
hash
cd ${ftp_file%/*}
ls
lcd ${local_file%/*}
lls
prompt
mget ${ftp_file##*/}
delete ${ftp_file##*/}
mput ${local_file##*/}
close
bye
EOF
echo "ftp success"